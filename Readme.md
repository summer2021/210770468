## Serverless架构环境划分方案探索

start-puppeteer
---

* Deploy puppeteer with s tool
  * Supported two runtimes: nodejs12 and custom container
  * Code open at https://github.com/devsapp/start-puppeteer/


![alt text](https://raw.githubusercontent.com/devsapp/start-puppeteer/master/src/demo.png)

fc-info unit test and integration test
---

* Add unit tests and integration tests for fc-info
* Integration test included no trigger, http and oss trigger
* Update publish.yaml with new properties (regions, service name, etc.)
* Update package.json with jest dependencies
* Remove interactive credential checking due to non-interactive test environment

#### Steps

```
npm install
npm test
npm run test-integration
```

#### Expected output

![image](https://user-images.githubusercontent.com/23239892/130613890-0ec92b98-8e84-46cf-8e5c-c49ebe60527b.png)


environment check api in fc-common
---

#### runtime = 'docker'
![docker](https://user-images.githubusercontent.com/23239892/132697460-7e462730-3690-471d-ab8a-4fedd229c204.png)

#### runtime = 'java8'
![java8](https://user-images.githubusercontent.com/23239892/132697418-5c51f552-9c08-4f47-a623-ef7809e94dcc.png)

#### runtime = 'nodejs12'
![nodejs12](https://user-images.githubusercontent.com/23239892/132697447-7ab9a2d6-8a36-40b8-8e5f-1daa4e955113.png)

#### runtime = 'python3'
![python3](https://user-images.githubusercontent.com/23239892/132697453-bd4977b7-a8db-43f3-a8d6-0e9b6caf5040.png)


Release Automation
---

WIP code here https://github.com/alapha23/start-component-with-ncc
The fork will pull request after thorough test.
